-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 21. Sep 2017 um 17:14
-- Server Version: 5.6.16
-- PHP-Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `ap_query`
--
DROP DATABASE IF EXISTS `ap_query`;
CREATE DATABASE IF NOT EXISTS `ap_query` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ap_query`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

DROP TABLE IF EXISTS `benutzer`;
CREATE TABLE IF NOT EXISTS `benutzer` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Benutzer` varchar(50) DEFAULT NULL,
  `Passwort` varchar(50) DEFAULT NULL,
  `GanzerName` varchar(50) DEFAULT NULL,
  `Benutzergruppe` int(11) DEFAULT NULL,
  `erfasst` datetime DEFAULT NULL,
  `deaktiviert` datetime DEFAULT NULL,
  `aktiv` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buchung`
--

DROP TABLE IF EXISTS `buchung`;
CREATE TABLE IF NOT EXISTS `buchung` (
  `BuchungID` int(11) NOT NULL AUTO_INCREMENT,
  `PersID` int(11) DEFAULT NULL,
  `Ankunft` datetime DEFAULT NULL,
  `Abreise` datetime DEFAULT NULL,
  `LandID` int(11) DEFAULT NULL,
  PRIMARY KEY (`BuchungID`),
  KEY `PersID` (`PersID`),
  KEY `LandID` (`LandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `land`
--

DROP TABLE IF EXISTS `land`;
CREATE TABLE IF NOT EXISTS `land` (
  `LandID` int(11) NOT NULL AUTO_INCREMENT,
  `Land` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`LandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `PersID` int(11) NOT NULL AUTO_INCREMENT,
  `Firma` varchar(50) DEFAULT NULL,
  `Vorname` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Strasse` varchar(50) DEFAULT NULL,
  `PLZ` varchar(15) DEFAULT NULL,
  `Ort` varchar(50) DEFAULT NULL,
  `Anrede` varchar(50) DEFAULT NULL,
  `Privattelefon` varchar(20) DEFAULT NULL,
  `Eingabedatum` datetime DEFAULT NULL,
  `Sprache` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PersID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `positionen`
--

DROP TABLE IF EXISTS `positionen`;
CREATE TABLE IF NOT EXISTS `positionen` (
  `PositionID` int(11) NOT NULL AUTO_INCREMENT,
  `BuchungID` int(11) DEFAULT NULL,
  `Konto` int(11) DEFAULT NULL,
  `Anzahl` int(11) DEFAULT NULL,
  `Preis` varchar(255) DEFAULT NULL,
  `Rabatt` double DEFAULT NULL,
  `MWStSatz` double DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Datum` datetime DEFAULT NULL,
  `Leistung` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`PositionID`),
  KEY `UserID` (`UserID`),
  KEY `BuchungID` (`BuchungID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `buchung`
--
ALTER TABLE `buchung`
  ADD CONSTRAINT `buchung_ibfk_2` FOREIGN KEY (`PersID`) REFERENCES `person` (`PersID`),
  ADD CONSTRAINT `buchung_ibfk_1` FOREIGN KEY (`LandID`) REFERENCES `land` (`LandID`);

--
-- Constraints der Tabelle `positionen`
--
ALTER TABLE `positionen`
  ADD CONSTRAINT `positionen_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `benutzer` (`UserID`),
  ADD CONSTRAINT `positionen_ibfk_1` FOREIGN KEY (`BuchungID`) REFERENCES `buchung` (`BuchungID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
